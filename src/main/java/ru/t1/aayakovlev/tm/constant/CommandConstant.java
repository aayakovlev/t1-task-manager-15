package ru.t1.aayakovlev.tm.constant;

public final class CommandConstant {

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String VERSION = "version";

    public final static String EXIT = "exit";

    public final static String INFO = "info";

    public final static String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    public final static String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    public final static String PROJECT_CLEAR = "project-clear";

    public final static String PROJECT_COMPLETE_BY_ID = "project-complete-by-id";

    public final static String PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";

    public final static String PROJECT_CREATE = "project-create";

    public final static String PROJECT_LIST = "project-list";

    public final static String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    public final static String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    public final static String PROJECT_SHOW_BY_ID = "project-show-by-id";

    public final static String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    public final static String PROJECT_START_BY_ID = "project-start-by-id";

    public final static String PROJECT_START_BY_INDEX = "project-start-by-index";

    public final static String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public final static String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public final static String TASK_BIND_TO_PROJECT = "task-bind-to-project";

    public final static String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    public final static String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    public final static String TASK_CLEAR = "task-clear";

    public final static String TASK_COMPLETE_BY_ID = "task-complete-by-id";

    public final static String TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

    public final static String TASK_CREATE = "task-create";

    public final static String TASK_LIST = "task-list";

    public final static String TASK_REMOVE_BY_ID = "task-remove-by-id";

    public final static String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    public final static String TASK_SHOW_BY_ID = "task-show-by-id";

    public final static String TASK_SHOW_BY_INDEX = "task-show-by-index";

    public final static String TASK_SHOW_BY_PROJECT_ID = "task-show-by-project-id";

    public final static String TASK_START_BY_ID = "task-start-by-id";

    public final static String TASK_START_BY_INDEX = "task-start-by-index";

    public final static String TASK_UNBIND_FROM_PROJECT = "task-unbind-from-project";

    public final static String TASK_UPDATE_BY_ID = "task-update-by-id";

    public final static String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    private CommandConstant() {
    }

}

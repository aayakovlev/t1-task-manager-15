package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectControllerImpl implements BaseController {

    private final ProjectService service;

    private final ProjectTaskService projectTaskService;

    public ProjectControllerImpl(final ProjectService service, final ProjectTaskService projectTaskService) {
        this.service = service;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void changeStatusById() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        service.changeStatusById(id, status);
    }

    @Override
    public void changeStatusByIndex() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        service.changeStatusByIndex(index, status);
    }

    @Override
    public void clear() throws AbstractException {
        System.out.println("[CLEAR PROJECTS]");
        service.deleteAll();
        System.out.println("[OK]");
    }

    @Override
    public void completeById() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        service.changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        service.changeStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void create() throws AbstractFieldException {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        service.create(name, description);
    }

    @Override
    public void removeById() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        service.findById(id);
        projectTaskService.removeProjectById(id);
    }

    @Override
    public void removeByIndex() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.findByIndex(index);
        projectTaskService.removeProjectById(project.getId());
    }

    private void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    private void show(final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL PROJECTS]");
        System.out.println("Sorts to enter:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = service.findAll(sort);
        renderProjects(projects);
    }

    @Override
    public void showById() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Project project = service.findById(id);
        show(project);
    }

    @Override
    public void showByIndex() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Project project = service.findByIndex(index);
        show(project);
    }

    @Override
    public void startById() throws AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        service.changeStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        service.changeStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void updateById() throws AbstractException {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        service.updateById(id, name, description);
    }

    @Override
    public void updateByIndex() throws AbstractException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        service.updateByIndex(index, name, description);
    }

}

package ru.t1.aayakovlev.tm.controller;

import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface ProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskFromProject() throws AbstractException;

}

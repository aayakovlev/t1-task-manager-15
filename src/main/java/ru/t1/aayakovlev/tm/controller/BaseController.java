package ru.t1.aayakovlev.tm.controller;

import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;

public interface BaseController {

    void changeStatusById() throws AbstractException;

    void changeStatusByIndex() throws AbstractException;

    void clear() throws AbstractException;

    void completeById() throws AbstractException;

    void completeByIndex() throws AbstractException;

    void create() throws AbstractFieldException;

    void removeById() throws AbstractException;

    void removeByIndex() throws AbstractException;

    void showAll();

    void showById() throws AbstractException;

    void showByIndex() throws AbstractException;

    void startById() throws AbstractException;

    void startByIndex() throws AbstractException;

    void updateById() throws AbstractException;

    void updateByIndex() throws AbstractException;

}

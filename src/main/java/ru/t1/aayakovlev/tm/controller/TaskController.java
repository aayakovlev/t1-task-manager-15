package ru.t1.aayakovlev.tm.controller;

import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;

public interface TaskController extends BaseController {

    void showByProjectId() throws AbstractFieldException;
}

package ru.t1.aayakovlev.tm.controller;

public interface CommandController {

    void showAbout();

    void showArgumentError();

    void showCommandError();

    void showExit();

    void showHelp();

    void showInfo();

    void showVersion();

    void showWelcome();

}

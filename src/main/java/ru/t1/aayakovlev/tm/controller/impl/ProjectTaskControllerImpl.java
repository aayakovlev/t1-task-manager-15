package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.ProjectTaskController;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectTaskControllerImpl implements ProjectTaskController {

    private final ProjectTaskService service;

    public ProjectTaskControllerImpl(final ProjectTaskService projectTaskService) {
        this.service = projectTaskService;
    }

    @Override
    public void bindTaskToProject() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("Enter project id: ");
        final String projectId = nextLine();
        System.out.print("Enter task id: ");
        final String taskId = nextLine();
        service.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject() throws AbstractException {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.print("Enter project id: ");
        final String projectId = nextLine();
        System.out.print("Enter task id: ");
        final String taskId = nextLine();
        service.unbindTaskFromProject(projectId, taskId);
    }

}

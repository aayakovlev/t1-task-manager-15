package ru.t1.aayakovlev.tm.controller.impl;

import ru.t1.aayakovlev.tm.controller.BaseController;
import ru.t1.aayakovlev.tm.controller.TaskController;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class TaskControllerImpl implements TaskController {

    private final TaskService service;

    public TaskControllerImpl(final TaskService service) {
        this.service = service;
    }

    @Override
    public void changeStatusById() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        service.changeStatusById(id, status);
    }

    @Override
    public void changeStatusByIndex() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.println("Statuses to enter:");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("Enter new status: ");
        final String statusValue = nextLine();
        final Status status = Status.toStatus(statusValue);
        service.changeStatusByIndex(index, status);
    }

    @Override
    public void clear() throws AbstractException {
        System.out.println("[CLEAR TASKS]");
        service.deleteAll();
    }

    @Override
    public void completeById() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        service.changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeByIndex() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        service.changeStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void create() throws AbstractFieldException {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        final String name = nextLine();
        System.out.print("Enter description: ");
        final String description = nextLine();
        service.create(name, description);
    }

    @Override
    public void removeById() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        service.findById(id);
        service.removeById(id);
    }

    @Override
    public void removeByIndex() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        service.findByIndex(index);
        service.removeByIndex(index);
    }

    private void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index++ + ". " + task);
        }
    }

    private void show(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Project id: " + task.getProjectId());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

    @Override
    public void showAll() {
        System.out.println("[SHOW ALL TASKS]");
        System.out.println("Sorts to enter:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = service.findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public void showById() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        final Task task = service.findById(id);
        show(task);
    }

    @Override
    public void showByIndex() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        final Task task = service.findByIndex(index);
        show(task);
    }

    @Override
    public void showByProjectId() throws AbstractFieldException {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("Enter projectId: ");
        final String projectId = nextLine();
        final List<Task> tasks = service.findByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public void startById() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        service.changeStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        service.changeStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void updateById() throws AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("Enter id: ");
        final String id = nextLine();
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        service.updateById(id, name, description);
    }

    @Override
    public void updateByIndex() throws AbstractException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("Enter index: ");
        final Integer index = nextNumber() - 1;
        System.out.print("Enter new name: ");
        final String name = nextLine();
        System.out.print("Enter new description: ");
        final String description = nextLine();
        service.updateByIndex(index, name, description);
    }

}

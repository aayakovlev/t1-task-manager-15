package ru.t1.aayakovlev.tm.service.impl;

import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ProjectService;

import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public final class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository repository;

    public ProjectServiceImpl(final ProjectRepository repository) {
        this.repository = repository;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) throws AbstractException {
        final int recordCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordCount) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project create(final String name) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public Project findById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) throws AbstractFieldException {
        final int recordsCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public Project removeById(final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) throws AbstractFieldException {
        final int recordsCount = repository.count();
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordsCount) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public void save(final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}

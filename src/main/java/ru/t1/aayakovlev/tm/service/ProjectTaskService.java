package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface ProjectTaskService {

    void bindTaskToProject(final String projectId, final String taskId) throws AbstractException;

    void removeProjectById(final String projectId) throws AbstractException;

    void unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException;

}

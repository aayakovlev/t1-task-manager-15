package ru.t1.aayakovlev.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Passed command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Passed command '" + command + "' not supported...");
    }

}

package ru.t1.aayakovlev.tm;

import ru.t1.aayakovlev.tm.component.Bootstrap;
import ru.t1.aayakovlev.tm.exception.system.ArgumentNotSupportedException;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.run(args);
        } catch (ArgumentNotSupportedException e) {
            System.out.println(e.getMessage());
        }
    }

}

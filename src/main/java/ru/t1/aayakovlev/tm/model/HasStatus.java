package ru.t1.aayakovlev.tm.model;

import ru.t1.aayakovlev.tm.enumerated.Status;

public interface HasStatus {

    Status getStatus();

    void setStatus(final Status status);

}

package ru.t1.aayakovlev.tm.model;

import java.util.Date;

public interface HasCreated {

    Date getCreated();

    void setCreated(final Date created);

}

package ru.t1.aayakovlev.tm.repository;

import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectRepository extends BaseRepository<Project> {

    Project create(final String name);

    Project create(final String name, final String description);

}

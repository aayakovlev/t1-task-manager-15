package ru.t1.aayakovlev.tm.repository;

import java.util.Comparator;
import java.util.List;

public interface BaseRepository<T> {

    int count();

    void deleteAll();

    boolean existsById(final String id);

    List<T> findAll();

    List<T> findAll(final Comparator<T> comparator);

    T findById(final String id);

    T findByIndex(final Integer index);

    T remove(final T e);

    T removeById(final String id);

    T removeByIndex(final Integer index);

    T save(final T e);

}
